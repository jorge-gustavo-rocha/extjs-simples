Ext.require(['Ext.grid.*', 'Ext.data.*', 'Ext.panel.*', 'Ext.layout.container.Border']);
Ext.onReady(function() {

	Ext.define('Contacto', {
		extend : 'Ext.data.Model',
		fields : ['id', 'nome', 'telefone', 'email']
	});

	var store = Ext.create('Ext.data.Store', {
		model : 'Contacto',
		autoLoad : true,
		proxy : {
			type : 'ajax',
			api : {
				read : 'php/listaContactos.php'
			},
			reader : {
				type : 'json',
				root : 'contactos',
				successProperty : 'success'
			}
		}
	});

	var grid = Ext.create('Ext.grid.Panel', {
		store : store,
		title : 'Contactos',
		columns : [{
			text : "Id",
			width : 40,
			dataIndex : 'id'
		}, {
			text : "Nome",
			flex : 1,
			dataIndex : 'nome',
			sortable : true,
			field : {
				xtype : 'textfield'
			}
		}, {
			text : "Telefone",
			width : 115,
			dataIndex : 'telefone',
			sortable : true,
			field : {
				xtype : 'textfield'
			}
		}, {
			text : "Email",
			width : 120,
			dataIndex : 'email',
			sortable : true,
			field : {
				xtype : 'textfield'
			}
		}],
		viewConfig : {
			forceFit : true
		},
		region : 'center'
	});

	var norte = Ext.widget('panel', {
		region : 'north',
		collapsible : true,
		title : 'North',
		split : true,
		height : 100,
		minHeight : 60,
		html : 'north'
	});

	var sul = Ext.widget('panel', {
		region : 'south',
		title : 'Rodapé',
		height : 40,
		minHeight : 60,
		html : 'Todos os direitos e as esquerdas reervadas à DRAPN'
	});
	
	Ext.create('Ext.container.Viewport', {
		frame : true,
		layout : 'border',
		items : [norte, grid, sul]
	});

});
